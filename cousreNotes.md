# Course Overview #

* introduce behavior-driven testing concepts
* mocks and Jasmine spies

### What is Jasmine? ###

* jasmine is a java script vtesting frame work
* abilities to create, and run tests, can produce test reports
* [Learn Markdown](https://jasmin.github.io)

### How do I get set up? ###

* create new file that ends with spec.ts
* "spec" allow the angular cli to complie our test
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact