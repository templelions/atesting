# Course Overview #

* introduce behavior-driven testing concepts
* mocks and Jasmine spies

### What is Jasmine? ###

* jasmine is a java script vtesting frame work
* abilities to create, and run tests, can produce test reports
* [Learn Markdown](https://jasmin.github.io)

### How do I get set up? ###

* create new file that ends with spec.ts
* "spec" allow the angular cli to complie our test
* to run -> ng test
* run without hot reload -> ng test --no--


### method examples ###

* spyOn - will monitor selected method
*  expect(logger.log).toHaveBeenCalledTimes(1); - test fasils if Log is called more then once.
* jasmine.createSpyObj - this will automaticaly impliment the spyOn method and holds the method you want tested

### Who do I talk to? ###

* adding x (ex: xdescribe('CalculatorService',) will disable the test 

* adding f (ex: describe('CalculatorService',) will will only profome this test , useful when debuging a single test.

* HttpClientTestingModule - provided in angular this impliment a mock server request

* afterEach - perform after tests
